(function () {
  'use strict';

  angular
    .module('scaffoldingmenu', [])
    .controller('scaffoldingmenuController', loadFunction);

  loadFunction.$inject = ['$q', '$rootScope', '$scope', 'structureService', '$location'];

  function loadFunction($q, $rootScope, $scope, structureService, $location) {
    // Register upper level modules
    structureService.registerModule($location, $scope, 'scaffoldingmenu');
    // --- Start scaffoldingmenuController content ---

    //defining variables
    $scope.menu;
    $scope.showArrowBack;
    $scope.menuIcon;
    $scope.menuStyle = {};

    var config;
    var withValue;

    init();
    //initialize variables and start logic module
    function init() {
      //initialize variables
      $scope.showArrowBack = false;
      $scope.menuIcon = true;
      config = $scope.scaffoldingmenu.modulescope;
      withValue = config.openPercent + "%";
      $scope.menu = getMenu();

      //set te menu position 
      menuPositionController(config.isPositionRight);

      //control if back button is visible and where show it
      arrowBackController(config.isPositionRight);
    }

    function getMenu() {
      var menu = [];
      //var trExp = /[\/\s]+/gi;

      angular.forEach(config.menuItems, function (value, key) {
        structureService.getModule(value.path).then(function (module) {
          var color = (value.bgColor) ? '#' + value.bgColor.replace('#', '') : '';
          var currentClass = ($location.path() === value.path) ? 'selectedpmenu' : '';
          menu.push({
            text: module.name,
            icon: getIcon(module.icon),
            typeicon: typeicon(module.icon),
            url: "#" + value.path,
            backgroundImage: value.bgImage,
            backgroundColor: color,
            class: currentClass
          });

        });
      });
      return menu;
    }

    $scope.headerText = function() {
      var headerText;
      if (config.headerText !== "") {
        headerText = config.headerText;
      } else {
        var pathmodule = $location.$$path;
        var moduleName;
        
        for(var i = 0; i<$scope.menu.length; i++){
          if ($scope.menu[i].url === "#" + pathmodule) {
            moduleName = $scope.menu[i].text;
          }
        }
        headerText = moduleName;
      }
      return headerText;
    }

    function getIcon(icon) {
      var finalIcon = "";

      if (config.showicons) {
        finalIcon = icon;
      }
      return finalIcon;
    }

    function typeicon(icon) {
      var typeicon;
      var regex = RegExp("^(https?://)?(([\\w!~*'().&=+$%-]+: )?[\\w!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([\\w!~*'()-]+\\.)*([\\w^-][\\w-]{0,61})?[\\w]\\.[a-z]{2,6})(:[0-9]{1,4})?((/*)|(/+[\\w!~*'().;?:@&=+$,%#-]+)+/*)$");

      if (icon && regex.test(icon)) {
        typeicon = 1;
      } else {
        typeicon = 0;
      }
      return typeicon;
    }

    $scope.goBack = function () {
      var tempUrl = angular.copy($rootScope.backUrl);
      if (tempUrl.length - 2 > -1) {
        $rootScope.backUrl.pop();
        $location.path(tempUrl[tempUrl.length - 2]);
      }
    };

    $scope.isSelected = function (url) {
      if ('#' + $location.$$path === url) {
        return true;
      }
    }

    $scope.checkbutton = function (buttonurl) {
      var fixButtonUrl = buttonurl.split("#")[1];
      if (fixButtonUrl === $location.url()) {
        $scope.closeMenu();
      }
    }


    $scope.search = function (action) {
      if (action && action === "show") {
        $scope.searchCamp = true;
        if ($scope.scaffoldingmenu.searchQuery && $scope.scaffoldingmenu.searchQuery !== undefined) {
          console.log($scope.scaffoldingmenu.searchQuery);
          $location.$$search = { 'q': $scope.scaffoldingmenu.searchQuery };
          $location.$$path = $location.$$path;
          $location.$$compose();
        }
      } else if (action && action === "hide") {
        $scope.searchCamp = false;
        if ($scope.scaffoldingmenu.searchQuery === undefined || $scope.scaffoldingmenu.searchQuery === "") {
          console.log($scope.scaffoldingmenu.searchQuery);
          $location.$$search = { 'q': "" };
          $location.$$path = $location.$$path;
          $location.$$compose();
        }
      } else {
        $scope.searchCamp = true;
      }
    }

    //menuPositionController function control the position of the menu
    function menuPositionController(positionRight) {
      if (positionRight) {
        $scope.headerStyles = { "flex-direction": "row" }
        $scope.menuStyle["right"] = 0;
        $scope.closebtn = { "right": "0" };
      } else {
        $scope.headerStyles = { "flex-direction": "row-reverse" }
        $scope.menuStyle["left"] = "0";
        $scope.closebtn = { "left": "0" };
      }
    }
    //control if back button is visible and where show it
    function arrowBackController(positionRight) {
      if (structureService.getMenuItems().indexOf($location.$$path) === -1 && $rootScope.current != 'scaffoldingmenu' && positionRight) {
        $scope.showArrowBack = true;
        $scope.menuIcon = false;
      } else if (structureService.getMenuItems().indexOf($location.$$path) === -1 && $rootScope.current != 'scaffoldingmenu' && !positionRight) {
        $scope.showArrowBack = true;
        $scope.searchStyle = {
          "margin-left": "35px"
        }
      }
    }

    $scope.closeMenu = function () {
      $scope.menuStyle['width'] = '0px';
      $scope.showScrim = false;
    }

    $scope.openMenu = function () {
      $scope.menuStyle['width'] = withValue;
      $scope.showScrim = true;
    }

    // --- End scaffoldingmenuController content ---
  }
}());
